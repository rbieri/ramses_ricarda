subroutine output_makefile(filename)
  character(LEN=80)::filename
  character(LEN=80)::fileloc
  character(LEN=30)::format
  integer::ilun

  ilun=11

  fileloc=TRIM(filename)
  format="(A)"
  open(unit=ilun,file=fileloc,form='formatted')
  write(ilun,format)"#############################################################################"
  write(ilun,format)"# If you have problems with this makefile, contact Romain.Teyssier@cea.fr"
  write(ilun,format)"#############################################################################"
  write(ilun,format)"# Compilation time parameters"
  write(ilun,format)"NVECTOR = 64"
  write(ilun,format)"NDIM = 3"
  write(ilun,format)"NPRE = 8"
  write(ilun,format)"NVAR = 10 # 5 usual hydro variable + 3 ionisation fractions (HI, HeI, HeII)"
  write(ilun,format)"NENER=1"
  write(ilun,format)"SOLVER = hydro"
  write(ilun,format)"PATCH = ../patch/rt/sinkRT"
  write(ilun,format)"EXEC = zoomTest"
  write(ilun,format)"ATON_FLAGS = #-DATON  # Uncomment to enable ATON."
  write(ilun,format)"NGROUPS = 5 # 5 if one ionising UV group"
  write(ilun,format)"#NGROUPS = 4 # 4 if one ionising UV group"
  write(ilun,format)"#NGROUPS = 3 # 3 if one ionising UV group"
  write(ilun,format)"#NGROUPS = 2 # 4 if one ionising UV group"
  write(ilun,format)"#NGROUPS = 1 # 1 if one ionising UV group"
  write(ilun,format)"DEFINES = -DNVECTOR=$(NVECTOR) -DNDIM=$(NDIM) -DNPRE=$(NPRE) -DNENER=$(NENER) -DNVAR=$(NVAR) \"
  write(ilun,format)"          -DSOLVER$(SOLVER) -DQUADHILBERT"
  write(ilun,format)"#############################################################################"
  write(ilun,format)"# Fortran compiler options and directives"
  write(ilun,format)""
  write(ilun,format)"# --- No MPI, gfortran -------------------------------"
  write(ilun,format)"#F90 = gfortran -O3 -frecord-marker=4 -fbacktrace -ffree-line-length-none -g"
  write(ilun,format)"#FFLAGS = -x f95-cpp-input $(DEFINES) -DWITHOUTMPI"
  write(ilun,format)""
  write(ilun,format)"# --- No MPI, tau ----------------------------------"
  write(ilun,format)"#F90 = tau_f90.sh -optKeepFiles -optPreProcess -optCPPOpts=$(DEFINES) -DWITHOUTMPI"
  write(ilun,format)""
  write(ilun,format)"# --- No MPI, pgf90 ----------------------------------"
  write(ilun,format)"#F90 = pgf90"
  write(ilun,format)"#FFLAGS = -Mpreprocess $(DEFINES) -DWITHOUTMPI"
  write(ilun,format)""
  write(ilun,format)"# --- No MPI, xlf ------------------------------------"
  write(ilun,format)"#F90 = xlf"
  write(ilun,format)"#FFLAGS = -WF,-DNDIM=$(NDIM),-DNPRE=$(NPRE),-DNVAR=$(NVAR),-DSOLVER$(SOLVER),-DWITHOUTMPI -qfree=f90 -qsuffix=f=f90 -qsuffix=cpp=f90"
  write(ilun,format)""
  write(ilun,format)"# --- No MPI, f90 ------------------------------------"
  write(ilun,format)"#F90 = f90"
  write(ilun,format)"#FFLAGS = -cpp $(DEFINES) -DWITHOUTMPI"
  write(ilun,format)""
  write(ilun,format)"# --- No MPI, ifort ----------------------------------"
  write(ilun,format)"#F90 = ifort"
  write(ilun,format)"#FFLAGS = -cpp $(DEFINES) -DWITHOUTMPI"
  write(ilun,format)""
  write(ilun,format)"# --- MPI, gfortran syntax ------------------------------"
  write(ilun,format)"#F90 = mpif90 -frecord-marker=4 -O3 -ffree-line-length-none -g -fbacktrace "
  write(ilun,format)"#FFLAGS = -x f95-cpp-input $(DEFINES)"
  write(ilun,format)""
  write(ilun,format)"# --- MPI, gfortran DEBUG syntax ------------------------------ "
  write(ilun,format)"#F90 = mpif90 -frecord-marker=4 -ffree-line-length-none -fbacktrace -g -O -fbounds-check -Wuninitialized -Wall"
  write(ilun,format)"#FFLAGS = -x f95-cpp-input -ffpe-trap=zero,underflow,overflow,invalid -finit-real=nan  $(DEFINES)    "
  write(ilun,format)""
  write(ilun,format)"# --- MPI, pgf90 syntax ------------------------------"
  write(ilun,format)"#F90 = mpif90 -O3"
  write(ilun,format)"#FFLAGS = -Mpreprocess $(DEFINES)"
  write(ilun,format)""
  write(ilun,format)"# --- MPI, ifort syntax ------------------------------"
  write(ilun,format)"F90 = mpif90"
  write(ilun,format)"FFLAGS = -cpp  $(DEFINES) -DNOSYSTEM -g -traceback -check bounds  #-fast"
  write(ilun,format)""
  write(ilun,format)""
  write(ilun,format)"# --- MPI, ifort syntax, additional checks -----------"
  write(ilun,format)"#F90 = mpif90"
  write(ilun,format)"#FFLAGS = -warn all -O0 -g -traceback -fpe0 -ftrapuv -check bounds -cpp $(DEFINES) -DNOSYSTEM"
  write(ilun,format)""
  write(ilun,format)""
  write(ilun,format)"# --- MPI, ifort syntax ------------------------------"
  write(ilun,format)"#F90 = ftn"
  write(ilun,format)"#FFLAGS = -xAVX -g -traceback -fpp -fast $(DEFINES) -DNOSYSTEM #-DRT "
  write(ilun,format)""
  write(ilun,format)"# --- MPI, ifort syntax, additional checks -----------"
  write(ilun,format)"#F90 = ftn"
  write(ilun,format)"#FFLAGS = -O3 -g -traceback -fpe0 -ftrapuv -cpp $(DEFINES) -DNOSYSTEM #-DRT"
  write(ilun,format)""
  write(ilun,format)"#############################################################################"
  write(ilun,format)"MOD = mod"
  write(ilun,format)"#############################################################################"
  write(ilun,format)"# MPI librairies"
  write(ilun,format)"LIBMPI = "
  write(ilun,format)"#LIBMPI = -lfmpi -lmpi -lelan"
  write(ilun,format)""
  write(ilun,format)"# --- CUDA libraries, for Titane ---"
  write(ilun,format)"LIBCUDA = -L/opt/cuda/lib  -lm -lcuda -lcudart"
  write(ilun,format)""
  write(ilun,format)"LIBGRACKLE = -lgrackle -lhdf5 -lz"
  write(ilun,format)"LIBS = $(LIBMPI)"
  write(ilun,format)"#############################################################################"
  write(ilun,format)"# Sources directories are searched in this exact order"
  write(ilun,format)"VPATH = $(PATCH):../$(SOLVER):../aton:../rt:../hydro:../pm:../poisson:../amr"
  write(ilun,format)"#############################################################################"
  write(ilun,format)"# All objects"
  write(ilun,format)"MODOBJ = amr_parameters.o amr_commons.o random.o pm_parameters.o pm_commons.o poisson_parameters.o \"
  write(ilun,format)"         poisson_commons.o hydro_parameters.o hydro_commons.o cooling_module.o bisection.o sparse_mat.o \"
  write(ilun,format)"         clfind_commons.o gadgetreadfile.o write_makefile.o write_patch.o write_gitinfo.o"
  write(ilun,format)""
  write(ilun,format)"AMROBJ = read_params.o init_amr.o init_time.o init_refine.o adaptive_loop.o amr_step.o update_time.o \"
  write(ilun,format)"         output_amr.o flag_utils.o physical_boundaries.o virtual_boundaries.o refine_utils.o nbors_utils.o \"
  write(ilun,format)"         hilbert.o load_balance.o title.o sort.o cooling_fine.o units.o light_cone.o movie.o"
  write(ilun,format)"# Particle-Mesh objects"
  write(ilun,format)"PMOBJ = init_part.o output_part.o rho_fine.o synchro_fine.o move_fine.o newdt_fine.o particle_tree.o \"
  write(ilun,format)"        add_list.o remove_list.o star_formation.o sink_particle.o feedback.o clump_finder.o clump_merger.o \"
  write(ilun,format)"        flag_formation_sites.o init_sink.o output_sink.o"
  write(ilun,format)"# Poisson solver objects"
  write(ilun,format)"POISSONOBJ = init_poisson.o phi_fine_cg.o interpol_phi.o force_fine.o multigrid_coarse.o multigrid_fine_commons.o \"
  write(ilun,format)"             multigrid_fine_fine.o multigrid_fine_coarse.o gravana.o boundary_potential.o rho_ana.o output_poisson.o"
  write(ilun,format)"# Hydro objects"
  write(ilun,format)"HYDROOBJ = init_hydro.o init_flow_fine.o write_screen.o output_hydro.o courant_fine.o godunov_fine.o \"
  write(ilun,format)"           uplmde.o umuscl.o interpol_hydro.o godunov_utils.o condinit.o hydro_flag.o hydro_boundary.o \"
  write(ilun,format)"           boundana.o read_hydro_params.o synchro_hydro_fine.o"
  write(ilun,format)"# RT objects"
  write(ilun,format)"RTOBJ = rt_init_hydro.o rt_init_xion.o rt_init.o rt_init_flow_fine.o rt_output_hydro.o rt_godunov_fine.o rt_interpol_hydro.o rt_godunov_utils.o rt_condinit.o rt_hydro_flag.o rt_hydro_boundary.o rt_boundana.o rt_read_hydro_params.o rt_units.o"
  write(ilun,format)"#AGN objects"
  write(ilun,format)"AGNOBJ = #stellar_wind.o init_stellar.o"
  write(ilun,format)"# All objects"
  write(ilun,format)"AMRLIB = $(AMROBJ) $(HYDROOBJ) $(PMOBJ) $(POISSONOBJ) $(AGNOBJ) $(RTOBJ)"
  write(ilun,format)"# ATON objects"
  write(ilun,format)"ATON_MODOBJ = timing.o radiation_commons.o rad_step.o"
  write(ilun,format)"ATON_OBJ = observe.o init_radiation.o rad_init.o rad_boundary.o rad_stars.o rad_backup.o ../aton/atonlib/libaton.a"
  write(ilun,format)"#############################################################################"
  write(ilun,format)"ramses:	$(MODOBJ) $(AMRLIB) ramses.o"
  write(ilun,format)"	$(F90) $(MODOBJ) $(AMRLIB) ramses.o -o $(EXEC)$(NDIM)d $(LIBS)"
  write(ilun,format)"	rm write_makefile.f90"
  write(ilun,format)"	rm write_patch.f90"
  write(ilun,format)"ramses_aton: $(MODOBJ) $(ATON_MODOBJ) $(AMRLIB) $(ATON_OBJ) ramses.o"
  write(ilun,format)"	$(F90) $(MODOBJ) $(ATON_MODOBJ) $(AMRLIB) $(ATON_OBJ) ramses.o -o $(EXEC)$(NDIM)d $(LIBS) $(LIBCUDA)"
  write(ilun,format)"	rm write_makefile.f90"
  write(ilun,format)"	rm write_patch.f90"
  write(ilun,format)"#############################################################################"
  write(ilun,format)"write_gitinfo.o: FORCE"
  write(ilun,format)"	$(F90) $(FFLAGS) -DPATCH=\'$(PATCH)\' -DGITBRANCH=\'$(GITBRANCH)\' -DGITHASH=\'""$(GITHASH)""\' \"
  write(ilun,format)" -DGITREPO=\'$(GITREPO)\' -DBUILDDATE=\'""$(BUILDDATE)""\' -c ../amr/write_gitinfo.f90 -o $@		"
  write(ilun,format)"write_makefile.o: FORCE"
  write(ilun,format)"	../utils/scripts/cr_write_makefile.sh $(MAKEFILE_LIST)"
  write(ilun,format)"	$(F90) $(FFLAGS) -c write_makefile.f90 -o $@"
  write(ilun,format)"write_patch.o: FORCE"
  write(ilun,format)"	../utils/scripts/cr_write_patch.sh $(PATCH)"
  write(ilun,format)"	$(F90) $(FFLAGS) -c write_patch.f90 -o $@"
  write(ilun,format)"%.o:%.f90"
  write(ilun,format)"	$(F90) $(FFLAGS) -c $^ -o $@"
  write(ilun,format)"FORCE:"
  write(ilun,format)"#############################################################################"
  write(ilun,format)"clean :"
  write(ilun,format)"	rm *.o *.$(MOD)"
  write(ilun,format)"#############################################################################"

  close(ilun)
end subroutine output_makefile
